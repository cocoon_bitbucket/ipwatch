package ipwatch_test



import(

	"github.com/alicebob/miniredis"
	"github.com/garyburd/redigo/redis"

	"time"
)

/*

	usage

	mock := NewMockRedis()
	defer mock.Close

	mock.Set("mockredis","ON")

	pool := mock.Pool()
	url := mock.Url()

 */




type MockRedis struct {
	* miniredis.Miniredis
	pool redis.Pool
	url string
}
func ( m * MockRedis) Close() {

	// close the redis pool
	m.pool.Close()

	// close the miniredis server
	m.Miniredis.Close()

	// reset url
	m.url = ""
}

func  ( m * MockRedis) Url() (redisUrl string){
	if m.url == "" {
		panic("Mockredis not initialized")
	}
	return m.url
}

func  ( m * MockRedis) Pool() (pool redis.Pool){
	if m.url == "" {
		panic("Mockredis not initialized")
	}
	return m.pool
}


func NewMockRedis() ( mini * MockRedis) {
	// return a miniredis and it s url

	s, err := miniredis.Run()
	if err != nil {
		panic(err)
	}

	// Optionally set some keys your code expects:
	s.Set("mockredis", "ON")
	url :=  "redis://" + s.Addr() + "/0"

	pool := getPool(url)

	mini = &MockRedis{s,pool,url}
	return mini

}

func getPool(redis_url string ) (pool redis.Pool){

	pool =  redis.Pool{
		MaxIdle: 3,
		//MaxActive: 50,
		IdleTimeout: 240 * time.Second,
		Dial: func () (redis.Conn, error) { return redis.DialURL(redis_url) },

		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
	return pool

}