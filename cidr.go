package ipwatch

import (
	"net"
	"log"
	"fmt"
)


type Cidr string


func incIp(ip net.IP) {
	// increment an IP
	for j := len(ip)-1; j>=0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func cloneIp(ip net.IP) net.IP {
	// duplicate an IP
	dup := make(net.IP, len(ip))
	copy(dup, ip)
	return dup
}


func CheckCidr(cidr string) (err error) {
	_, _, err = net.ParseCIDR(cidr)
	return err
}


func (cidr Cidr) IpList() ([]string,error) {

	ip, ipnet, err := net.ParseCIDR(string(cidr))
	if err != nil { return nil, err }

	var ips []string
	for ip := ip.Mask(ipnet.Mask); ipnet.Contains(ip); incIp(ip) {
		ips = append(ips, ip.String())
	}
	// remove network address and broadcast address
	return ips[1 : len(ips)-1], nil
}



func (cidr Cidr) Range() chan net.IP {
	//
	// return a channel of IP from a cidr , iterate each ip of the cidr
	//

	c := make(chan net.IP)
	go func() {

		defer close(c)

		ip, ipnet, err := net.ParseCIDR(string(cidr))
		if err != nil {
			log.Fatal(err)
		}
		iip := ip.Mask(ipnet.Mask)
		for {
			if ipnet.Contains(iip) {
				//fmt.Println("* ", iip)
				dup := cloneIp(iip)
				c <- dup
				incIp(iip)
				//time.Sleep(1 * time.Millisecond)

			} else {
				break
			}
		}
	}()
	return c
}


func (cidr Cidr) RangeNetAddr(ports []int) ( c chan net.TCPAddr, err error) {
	//
	// return a channel of net.TCPAddr  (ip:port) from a cidr, and port list  and
	//
	// iterate each ip:port of the cidr/ports
	//
	// check cidr
	_, _, err = net.ParseCIDR(string(cidr))
	if err != nil {
		return c,err
	}

	c = make(chan net.TCPAddr)
	go func() {
		defer close(c)

		ip, ipnet, err := net.ParseCIDR(string(cidr))
		if err != nil {
			return
		}
		iip := ip.Mask(ipnet.Mask)
		for {
			if ipnet.Contains(iip) {
				//fmt.Println("* ", iip)
				dup := cloneIp(iip)
				for _,port := range (ports) {
					uri := fmt.Sprintf("%s:%d", dup.String(), port)
					tcpAddr, err := net.ResolveTCPAddr("tcp4", uri)
					if err == nil {
						c <- * tcpAddr
					}
				}
				incIp(iip)
				//time.Sleep(1 * time.Millisecond)

			} else {
				break
			}
		}
	}()
	return c,err
}

