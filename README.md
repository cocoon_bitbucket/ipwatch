ipwatch a server to scan open ports on the lan


# overview

ipwatch has 2 parts ipwatch server and ipwatch client


# ipwatch server

a task running in the backgroud and scan subnets on the lan 

the open ports discovered are registered in a redis db

ipwatch scan_tasks are base on a subnet ( specified as a cidr eg 192.168.1.1/24) and a list ports (80,23 ..)

ipwatch server will launch a task for each subnet declared



# ipwatch client

an api to retrieve the open ports from the redis db


# usage

## start a ipwatch server


    m,err := ipwatch.NewIpwatchManager( redis_pool, period_in_second , log_level)
    m.Run()
    

## use ipwatch client

    c := ipwatch.IpwatchClient{redis_pool}
    
    c.AddIpRange("192.168.1.1/24", []int{80,23}
    
    netAddr,err := c.RangeOpenPorts("192.168.1.1/24",[]int{80,23})
    
    for addr := range netAddr {
        fmt.Printf("addr= %s\n",addr.String())
        // eg "192.168.1.1:23" 
    }
