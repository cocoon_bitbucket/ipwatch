package ipwatch_test

import(

	"log"
	"testing"
	"bitbucket.org/cocoon_bitbucket/ipwatch"
	"time"
	"os"
	"os/signal"
	"golang.org/x/net/context"
	"github.com/magiconair/properties/assert"
)

func cidr_config() ipwatch.IpwatchConfig{

	cnf := ipwatch.IpwatchConfig{}
	cnf.Period,_ = time.ParseDuration("60s")     // period in second
	cnf.Ttl,_= time.ParseDuration("120s")         // time to live in redis db in second
	cnf.Timeout = 50   // wait for tcp connect in millisecond
	cnf.LogLevel = 10   // log_level  0:no log : 10 , success connections , 90 : all

	p := make( map[ipwatch.Cidr]ipwatch.Ports)
	//var ports ipwatch.Ports = [80,23]
	p["192.168.1.0/24"] = []int{80,23}
	cnf.IpRanges = p

	return cnf
}




func TestServer(t *testing.T) {

	// subscribe to SIGINT signals
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)


	mock := NewMockRedis()
	defer mock.Close()
	url := mock.Url()
	//url := "redis://127.0.0.1:6379/0"

	ipwatch.InitRedis(url)
	defer ipwatch.UnsetRedis()


	// start ipwatch server
	pool,err := ipwatch.GetPool()
	if err == nil {
		m, err := ipwatch.NewIpwatchManager(pool.Pool, 10000000000, 90)
		if err == nil {

			// set config
			cnf := cidr_config()
			cnx := pool.Get()
			defer cnx.Close()
			err := cnf.Set(cnx)
			assert.Equal(t,nil,err)
			if err != nil { return }


			go m.Run()
		}
	}
	assert.Equal(t,nil,err)

	log.Printf("main: starting HTTP server")

	srv := ipwatch.StartHttpServer(":8080")

	log.Printf("main:  serving for 10 seconds")


	// wait for  10 s or SIGINT
	//time.Sleep( 10 * time.Second)
	<-stopChan


	log.Println("Shutting down server...")

	// shut down gracefully, but wait no longer than 5 seconds before halting
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	srv.Shutdown(ctx)

	log.Println("Server gracefully stopped")


	log.Printf("main: done. exiting")


}


