package main

import(
	"flag"
	"log"
	"bitbucket.org/cocoon_bitbucket/ipwatch"
	"time"
	"os"
	"os/signal"
	"golang.org/x/net/context"

)


var (

	redisUrl= flag.String("redis-url","redis://127.0.0.1:6379/0","redis url")

)


func cidr_config() ipwatch.IpwatchConfig{

	cnf := ipwatch.IpwatchConfig{}
	cnf.Period,_ = time.ParseDuration("60s")     // period in second
	cnf.Ttl,_= time.ParseDuration("120s")         // time to live in redis db in second
	cnf.Timeout = 50   // wait for tcp connect in millisecond
	cnf.LogLevel = 10   // log_level  0:no log : 10 , success connections , 90 : all

	p := make( map[ipwatch.Cidr]ipwatch.Ports)
	//var ports ipwatch.Ports = [80,23]
	p["192.168.1.0/24"] = []int{80,23}
	cnf.IpRanges = p

	return cnf
}




func main() {

	// subscribe to SIGINT signals
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)

	flag.Parse()

	// init Redis
	ipwatch.InitRedis(*redisUrl)
	defer ipwatch.UnsetRedis()

	// init metrics
	ipwatch.InitMetrics()

	period,err := time.ParseDuration("20s")
	if err != nil {
		period = 20*time.Second
	}

	// start ipwatch server
	pool,err := ipwatch.GetPool()
	if err == nil {
		m, err := ipwatch.NewIpwatchManager(pool.Pool, period, 90)
		if err == nil {

			// set config
			cnf := cidr_config()
			cnx := pool.Get()
			defer cnx.Close()
			err := cnf.Set(cnx)
			if err != nil { return }

			go m.Run()
		}
	}

	log.Printf("main: starting HTTP server")

	srv := ipwatch.StartHttpServer(":8080")

	log.Printf("main:  serving for %s", period.String() )


	// wait for  10 s or SIGINT
	//time.Sleep( 10 * time.Second)
	<-stopChan


	log.Println("Shutting down server...")

	// shut down gracefully, but wait no longer than 5 seconds before halting
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	srv.Shutdown(ctx)

	log.Println("Server gracefully stopped")


	log.Printf("main: done. exiting")


}

