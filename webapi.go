package ipwatch


import (
	"net/http"
	"io"
	"strings"
	"log"
	"fmt"
	"encoding/json"
	"io/ioutil"
	"errors"
	"strconv"
)


/*

	web api of ipwatch

	 POST /ipwatch/cidr    { cidr= 192.168.1.0/24 , ports=[80,23] , log_level=10 , timeout=2 } => 201
	 GET /ipwatch/cidr     => 200  dict of cidr

	 DEL /ipwatch/cidr/192.168.1.0-24   delete cidr 192.168.1.0:24


	// get open ports
	GET /ipwatch/live  ?cidr= ports=



 */


type Response struct {

	Error error         `json:"error"`
	Result interface{}	`json:"result"`
}


//func (r * Response) Marshall() ( text string, err error){
//
//	b,err := json.Marshal(&r)
//	if err != nil {
//		text =
//		return text,err
//	} else {
//		r.Error = nil
//	}
//
//	//return err
//}

type PostCidrMessage struct {
	Ip string
	Mask string
	Ports string
}





func CidrHandler(w http.ResponseWriter, r *http.Request) {


	parts := strings.Split(r.URL.Path,"/")
	log.Print(r.URL.Path)
	log.Printf("%s\n",parts)


	//response := &Response{}

	switch r.Method {
	case "GET":

		switch len(parts) {
		//case 3:
		//	// collection url => LIST
		//	io.WriteString(w, "ipwatch/cidr")
		case 4:
			item := parts[3]
			if item == "" {
				// collection url => LIST
				io.WriteString(w, "ipwatch/cidr")
			} else {
				// item url
				io.WriteString(w, "ipwatch/cidr/?")
			}
		default:
			io.WriteString(w, "ipwatch/cidr bad url schema")

		}

	case "POST":
		// POST ipwatch/cidr  { ip: , mask:24 , ports:80,23 }

		// decode json message
		jsn, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatal("Error reading the body", err)
		}
		data := &PostCidrMessage{}
		err = json.Unmarshal(jsn, &data)
		if err != nil {
			log.Fatal("Decoding error: ", err)
		}

		// Do operation
		err = AddRange(data)
		result := true
		if err != nil { result = false }

		// send response
		w.Header().Set("Content-Type", "application/json")
		response := &Response{err,result}

		if err := json.NewEncoder(w).Encode(response); err != nil {
			panic(err)
		}


	default:
		//response.Error = http.StatusMethodNotAllowed
		io.WriteString(w, "ipwatch/cidr Method Not allowed")

	}

}



// ipwatch/live
func LiveHandler(w http.ResponseWriter, r *http.Request) {
	// list all open ports

	w.Header().Set("Content-Type", "application/json")
	response := &Response{}

	//io.WriteString(w, "ipwatch/live\n")
	response.Result,response.Error = ListOpenPorts()
	if err := json.NewEncoder(w).Encode(response); err != nil {
		panic(err)
	}


}



///
///

func ListOpenPorts() (ports []string,err error){

	pool,err := GetPool()
	if err != nil { return ports ,err}

	c := IpwatchClient{pool.Pool}

	//c.AddIpRange("192.168.1.1/24", []int{80,23}

	netAddr,err := c.RangeOpenPorts("192.168.1.1/24",[]int{80,23})
	if err != nil { return ports,err}
	for addr := range netAddr {
		fmt.Printf("addr= %s\n",addr.String())
		ports= append(ports,addr.String())
	}

	return ports,err
}


func AddRange( message * PostCidrMessage ) ( err error){

	pool,err := GetPool()
	if err != nil { return err}


	if message.Mask == "" {
		message.Mask = "24"
	}

	// handle ports
	ports := []int{}
	if message.Ports == "" {
		err = errors.New("No port(s) specified")
		return err
	}
	parts := []string{}
	if strings.Contains(message.Ports,",") {
		parts = strings.Split(message.Ports ,",")
	} else {
		parts[0]= message.Ports
	}
	for _,part := range parts{
		v,err := strconv.Atoi(part)
		if err != nil {
			log.Printf("AddRange: cannot convert to int: %s",part)
			continue
		}
		ports = append(ports,v)
	}

	c := IpwatchClient{pool.Pool}

	err = c.AddIpRange( message.Ip + "/" + message.Mask, ports)

	return err
}
