package ipwatch

import (
	"math/rand"
	"github.com/prometheus/client_golang/prometheus"
)

/*

	metrics
	ipwatch_latency ip=
	ipwatch_uptime
	ipwatch_opened


 */


var (
	// Create a summary to track fictional interservice RPC latencies for three
	// distinct services with different latency distributions. These services are
	// differentiated via a "service" label.
	rpcDurations = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name:       "rpc_durations_seconds",
			Help:       "RPC latency distributions.",
			Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		},
		[]string{"service"},
	)

	TcpConnectionTime = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name:       "tcp_connection_time",
			Help:       "time to established a tcp connection to port.",
		},
		[]string{"ip","port"},
	)


)


func InitMetrics() {

	// Register the summary and the histogram with Prometheus's default registry.
	prometheus.MustRegister(rpcDurations)
	prometheus.MustRegister(TcpConnectionTime)
}



func sample_feed() {

	v := rand.Float64()
	rpcDurations.WithLabelValues("uniform").Observe(v)
	TcpConnectionTime.WithLabelValues("192.168.1.1","80").Set(v)

}