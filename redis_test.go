package ipwatch_test


import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/ipwatch"
	//"fmt"
	"github.com/magiconair/properties/assert"
	"strings"
)


func TestInitRedis(t *testing.T) {


	p := ipwatch.RedisPool{}
	assert.Equal(t,false,p.IsSet)

	p.IsSet = true

	assert.Equal(t,true,p.IsSet)


	_,err := ipwatch.GetPool()
	if err == nil {
		print("error")
	}

	mock := NewMockRedis()
	defer mock.Close()
	url := mock.Url()
	//url := "redis://127.0.0.1:6379/0"


	err = ipwatch.InitRedis(url)
	assert.Equal(t,nil,err)

	pool,err := ipwatch.GetPool()
	assert.Equal(t,nil,err)

	cnx := pool.Get()
	defer cnx.Close()

	_,err = cnx.Do("PING")
	assert.Equal(t,nil,err)


	err = ipwatch.InitRedis("redis://127.0.0.1:6379/0")
	assert.Equal(t,strings.Contains(err.Error(),"Pool already "),true)


	cnx2 := pool.Get()
	defer cnx2.Close()


	ipwatch.UnsetRedis()


	_,err = ipwatch.GetCnx()
	assert.Equal(t,strings.Contains(err.Error(),"No Pool "),true)


	return

}