package ipwatch


import(
	"github.com/garyburd/redigo/redis"
	"time"
	"errors"
	"log"
)



type RedisPool struct {
	redis.Pool
	Url string
	IsSet bool
}




var (

	redis_pool RedisPool
)

//
// setup RedisUrl , create a Pool
//
func InitRedis(redis_url string) (err error) {

	if redis_pool.IsSet {
		err = errors.New("Pool already initialized")
		return err
	}
	p := MakePool(redis_url)
	redis_pool = RedisPool{ p, redis_url,true}
	log.Print("Redis Pool created for " + redis_pool.Url)
	return err
}

func UnsetRedis(){
	redis_pool = RedisPool{}
}



func GetPool() ( pool RedisPool, err error) {
	// get the pool initialized by InitRedis
	if  redis_pool.IsSet {
		return redis_pool,err
	} else {
		err = errors.New("No Pool Initialized")
		return pool,err
	}
}

func GetCnx() ( cnx redis.Conn, err error) {
	// get the pool initialized by InitRedis
	pool,err := GetPool()
	if err != nil { return cnx,err}
	cnx = pool.Get()
	return cnx ,err
}



//
//
//

func MakePool( redis_url string) redis.Pool {

	pool :=  redis.Pool{
		MaxIdle: 3,
		//MaxActive: 50,
		IdleTimeout: 240 * time.Second,
		Dial: func () (redis.Conn, error) { return redis.DialURL(redis_url) },

		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
	return pool
}
