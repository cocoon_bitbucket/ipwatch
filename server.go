package ipwatch



import (
	"log"
	"net/http"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"io"
)



func StartHttpServer( addr string) *http.Server {

	srv := &http.Server{Addr: addr}

	// home page
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "hello world\n")
	})

	// Expose the registered metrics via HTTP.
	http.Handle("/metrics", promhttp.Handler())


	// ipwatch/cidr
	http.HandleFunc("/ipwatch/cidr/", CidrHandler)

	// ipwatch/live
	http.HandleFunc("/ipwatch/live/", LiveHandler)



	log.Printf("Httpserver: ListenAndServe() addr: %s", addr)
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			// cannot panic, because this probably is an intentional close
			log.Printf("Httpserver: ListenAndServe() error: %s", err)
		}
	}()

	// returning reference so caller can call Shutdown()
	return srv
}




