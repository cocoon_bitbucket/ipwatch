

package ipwatch_test


import(

	"testing"
	"log"
	"github.com/garyburd/redigo/redis"
	"github.com/stretchr/testify/assert"
	//"sync"
	"net"

	//"fmt"
	//"time"
	//"fmt"

	"fmt"
	"time"
	"bitbucket.org/cocoon_bitbucket/ipwatch"
)


var (

	redisMode = "MOCK"
)


func get_pool() redis.Pool {

	pool := ipwatch.MakePool("redis://localhost:6379/0")

	// reset DB
	cnx := pool.Get()
	_,err := cnx.Do("FLUSHDB")
	if err == nil {
		cnx.Close()
	}
	return pool
}


func get_cnf2() ipwatch.IpwatchConfig{

	cnf := ipwatch.IpwatchConfig{}
	cnf.Period,_ = time.ParseDuration("60s")     // period in second
	cnf.Ttl,_= time.ParseDuration("120s")         // time to live in redis db in second
	cnf.Timeout = 50   // wait for tcp connect in millisecond
	cnf.LogLevel = 10   // log_level  0:no log : 10 , success connections , 90 : all

	p := make( map[ipwatch.Cidr]ipwatch.Ports)
	//var ports ipwatch.Ports = [80,23]
	p["192.168.1.1/24"] = []int{80,23}
	p["192.168.200.1/24"] = []int{80}
	cnf.IpRanges = p

	return cnf
}



func TestIpwatchConfig(t *testing.T) {

	cnf := get_cnf2()

	mock := NewMockRedis()
	defer mock.Close()

	//pool := get_pool()
	pool := mock.Pool()


	cnx := pool.Get()
	defer cnx.Close()

	err := cnf.Set(cnx)
	assert.Equal(t,nil,err)
	if err != nil { return}


	lcnf := ipwatch.IpwatchConfig{}
	err = lcnf.Load(cnx)
	assert.Equal(t,nil,err)
	if err != nil { return}

	assert.Equal(t,cnf,lcnf)


	err = lcnf.AddIpRange(cnx,"192.168.300.0/24",[]int{80})
	assert.Equal(t,nil,err)

	err = lcnf.Load(cnx)
	assert.Equal(t,nil,err)

	assert.Equal(t,3,len(lcnf.IpRanges))

	err = lcnf.DelIpRange(cnx,"192.168.300.0/24")
	assert.Equal(t,nil,err)

	err = lcnf.Load(cnx)
	assert.Equal(t,nil,err)

	assert.Equal(t,2,len(lcnf.IpRanges))



}

func TestIpwatchCidrState(t *testing.T) {

	mock := NewMockRedis()
	defer mock.Close()
	pool := mock.Pool()


	cnx := pool.Get()
	defer cnx.Close()

	state,err := ipwatch.NewIpIpwatchCidrState("192.168.1.0/24",[]int{80,23},200,60,120,90)

	err = state.Set(cnx)
	assert.Equal(t,nil,err)
	if err != nil { return}

	lstate := ipwatch.IpwatchCidrState{}
	err = lstate.Load(cnx,"192.168.1.0/24")

	assert.Equal(t,nil,err)
	if err != nil { return}
	//
	assert.Equal(t,state,lstate)


	err= lstate.SetStatus(cnx,"192.168.1.0/24","Done",60)
	assert.Equal(t,nil,err)

	status,err := lstate.GetStatus(cnx,"192.168.1.0/24")
	assert.Equal(t,nil,err)
	assert.Equal(t,"Done",status)

}






func TestIpwatcher (t *testing.T) {

	//cidr := "192.168.1.1/24"

	cnf := ipwatch.IpwatchConfig{}
	cnf.Period = 10
	cnf.Ttl= 19

	p := make( map[ipwatch.Cidr]ipwatch.Ports)
	//var ports ipwatch.Ports = [80,23]
	p["192.168.1.1/24"] = []int{80,23}
	p["192.168.200.1/24"] = []int{80}
	cnf.IpRanges = p

	mock := NewMockRedis()
	defer mock.Close()
	pool := mock.Pool()

	cnx := pool.Get()

	_,err := ipwatch.NewIpWatcher(pool,nil)
	assert.Equal(t,nil,err)

	//err = db.SetConfig(cnf)
	err = cnf.Set(cnx)
	assert.Equal(t,nil,err)

	s := ipwatch.IpwatchConfig{}
	err = s.Load(cnx)
	assert.Equal(t,nil,err)

	//assert.Equal(t,s,cnf)

	assert.Equal(t,10*time.Second,s.Period * 1000000000)
	assert.Equal(t,19*time.Second,s.Ttl *  1000000000)

	assert.Equal(t,80,s.IpRanges["192.168.1.1/24"][0])
	assert.Equal(t,23,s.IpRanges["192.168.1.1/24"][1])

	assert.Equal(t,80,s.IpRanges["192.168.200.1/24"][0])

	//fmt.Printf("%s\n",s)

	return
}


func TestIpwatcherSetPort(t *testing.T) {

	mock := NewMockRedis()
	defer mock.Close()
	pool := mock.Pool()

	db,err := ipwatch.NewIpWatcher(pool,nil)
	assert.Equal(t,nil,err)


	uri := "192.168.1.1:80"
	tcpAddr, err := net.ResolveTCPAddr("tcp4", uri)
	assert.Equal(t,nil,err)

	// set port as open
	err = db.SetPort(tcpAddr,1,0)
	assert.Equal(t,nil,err)

	// read port: we expect port is open so err==nil
	err= db.GetPort(tcpAddr)
	assert.Equal(t,nil,err)

	// wait for end of ttl
	//time.Sleep(2 * time.Second)
	mock.FastForward(2 * time.Second)

	// read port: we expect port has expired so err!= nil
	err= db.GetPort(tcpAddr)
	assert.NotEqual(t,nil,err)
	//assert.Equal(t,"NotFound",err.Error())


	return
}

func TestGetIpwatchIpKeys(t *testing.T) {

	//cidr := "192.168.1.1/24"

	mock := NewMockRedis()
	defer mock.Close()
	pool := mock.Pool()

	w, err := ipwatch.NewIpWatcher(pool,nil)
	assert.Equal(t, nil, err)

	// create mock open ports
	a1, err := net.ResolveTCPAddr("tcp4", "192.168.1.1:80")
	w.SetPort(a1, 100,0)
	a2, err := net.ResolveTCPAddr("tcp4", "192.168.1.1:23")
	w.SetPort(a2, 100,0)

	a3, err := net.ResolveTCPAddr("tcp4", "192.168.1.30:80")
	w.SetPort(a3, 100,0)

	a4, err := net.ResolveTCPAddr("tcp4", "192.168.100:80")
	w.SetPort(a4, 100,0)


	// get open open ports
	//ports := []int{80,23}

	cnx := w.Pool.Get()


	start := time.Now()
	keys ,err := ipwatch.GetDbIpwatchIp( cnx, "192.168.1.1/24")
	elapsed := time.Since(start)
	log.Printf("GetDbIpwatchIp took %s\n", elapsed)

	assert.True(t, len(keys) == 3)
	//for _,port := range all {
	//	fmt.Printf("Found open port: %s\n", port.String())
	//}
}



func TestGetOpenPortsOnMock(t *testing.T) {

	//cidr := "192.168.1.1/24"

	mock := NewMockRedis()
	defer mock.Close()
	pool := mock.Pool()

	w, err := ipwatch.NewIpWatcher(pool,nil)
	assert.Equal(t, nil, err)

	// create mock open ports
	a1, err := net.ResolveTCPAddr("tcp4", "192.168.1.1:80")
	w.SetPort(a1, 100,0)
	a2, err := net.ResolveTCPAddr("tcp4", "192.168.1.1:23")
	w.SetPort(a2, 100,0)

	a3, err := net.ResolveTCPAddr("tcp4", "192.168.1.30:80")
	w.SetPort(a3, 100,0)


	// get open open ports
	ports := []int{80,23}

	cnx := w.Pool.Get()


	start := time.Now()
	//open_ports := ipwatch.GetOpenPorts( cnx, "192.168.1.1/24", ports)
	open_ports,_ := ipwatch.RangeOpenPorts( cnx, "192.168.1.1/24", ports)

	all := []net.TCPAddr{}
	for port := range open_ports {
		all = append(all, port)
	}
	elapsed := time.Since(start)
	log.Printf("GetOpenPorts took %s\n", elapsed)

	assert.True(t, len(all) == 3)
	//for _,port := range all {
	//	fmt.Printf("Found open port: %s\n", port.String())
	//}
}

func TestGetOpenPortsFromKeysOnMock(t *testing.T) {

	//cidr := "192.168.1.1/24"

	mock := NewMockRedis()
	defer mock.Close()
	pool := mock.Pool()

	w, err := ipwatch.NewIpWatcher(pool,nil)
	assert.Equal(t, nil, err)

	// create mock open ports
	a1, err := net.ResolveTCPAddr("tcp4", "192.168.1.1:80")
	w.SetPort(a1, 100,0)
	a2, err := net.ResolveTCPAddr("tcp4", "192.168.1.1:23")
	w.SetPort(a2, 100,0)

	a3, err := net.ResolveTCPAddr("tcp4", "192.168.1.30:80")
	w.SetPort(a3, 100,0)

	a4, err := net.ResolveTCPAddr("tcp4", "192.168.100:80")
	w.SetPort(a4, 100,0)


	// get open open ports
	ports := []int{80,23}

	cnx := w.Pool.Get()


	start := time.Now()
	//open_ports,err := ipwatch.GetOpenPortsFromKeys( cnx, "192.168.1.1/24", ports)
	open_ports,err := ipwatch.RangeOpenPorts( cnx, "192.168.1.1/24", ports)
	assert.Equal(t,nil,err)
	all := []net.TCPAddr{}
	for port := range open_ports {
		all = append(all, port)
	}
	elapsed := time.Since(start)
	log.Printf("GetOpenPorts took %s\n", elapsed)

	assert.True(t, len(all) == 3 )
	for _,port := range all {
		fmt.Printf("Found open port: %s\n", port.String())
	}
}




//func TestIpwatchSubnetTask (t *testing.T) {
//
//	//cidr := "192.168.1.1/24"
//
//
//	//cnf := get_cnf()
//	//cnx := get_db()
//	pool := get_pool()
//
//	cnx := pool.Get()
//	defer cnx.Close()
//
//
//	timeout := 20   // in millisecond
//
//
//	task,err := ipwatch.NewIpIpwatchCidrState("192.168.1.1/24",[]int{80,23},timeout,0,120,10)
//	assert.Equal(t,nil,err)
//	if err != nil { return}
//
//	task.Set(cnx)
//
//	w,err := ipwatch.NewIpWatcher(pool,&task)
//	assert.Equal(t,nil,err)
//
//
//
//	start := time.Now()
//	err = w.WatchSubnetTask()
//	elapsed := time.Since(start)
//	log.Printf("WatchSubnetTask took %s\n", elapsed)
//
//	//err = ipwatch.WatchSubnetTask( &w, cnf, "192.168.200.1/24")
//	//err = ipwatch.WatchSubnetTask( &w, cnf, "192.168.300.1/24")
//	assert.Equal(t,nil,err)
//
//	// check status
//	status,err := w.GetStatus()
//	assert.Equal(t,nil,err)
//	assert.Equal(t,"Done",status)
//
//
//	// get open open ports
//	ports := []int{80,23}
//
//	_,err = cnx.Do("PING")
//	assert.Equal(t,nil,err)
//
//	//defer cnx.Close()
//	//open_ports := ipwatch.GetOpenPorts(cnx, "192.168.1.1/24", ports)
//	open_ports,err := ipwatch.RangeOpenPorts(cnx, "192.168.1.1/24", ports)
//	assert.Equal(t,nil,err)
//
//	if err == nil {
//		all := []net.TCPAddr{}
//		start = time.Now()
//		for port := range open_ports {
//			all = append(all, port)
//		}
//		elapsed = time.Since(start)
//		log.Printf("RangeOpenPorts took %s\n", elapsed)
//
//		assert.True(t, len(all) >= 1)
//		for _, port := range all {
//			fmt.Printf("Found open port: %s\n", port.String())
//		}
//	}
//	return
//}





//func TestIpwatchManager (t *testing.T) {
//
//	cnf  := get_cnf2()
//	pool := get_pool()
//
//	cnx := pool.Get()
//	defer cnx.Close()
//
//
//	// set config
//	err := cnf.Set(cnx)
//	assert.Equal(t,nil,err)
//	if err != nil { return }
//
//
//	log_level := 10
//	period := 10 * time.Second
//
//
//	m,err := ipwatch.NewIpwatchManager(pool,period, log_level)
//
//	assert.Equal(t,nil,err)
//	if err != nil { return }
//
//
//	m.Run()
//
//}



func TestIpwatchClient (t *testing.T) {


	//var redisUrl= "redis://localhost:6379/0"

	mock := NewMockRedis()
	defer mock.Close()
	pool := mock.Pool()

	//pool :=  redis.Pool{
	//	MaxIdle: 3,
	//	//MaxActive: 50,
	//	IdleTimeout: 240 * time.Second,
	//	Dial: func () (redis.Conn, error) { return redis.DialURL(redisUrl) },
	//
	//	TestOnBorrow: func(c redis.Conn, t time.Time) error {
	//		_, err := c.Do("PING")
	//		return err
	//	},
	//}
	cnx := pool.Get()
	defer cnx.Close()

	c := ipwatch.IpwatchClient{pool}

	netAddr,err := c.RangeOpenPorts("192.168.1.1/24",[]int{80})
	assert.Equal(t,nil,err)
	if err != nil { return }
	_= netAddr
	for addr := range netAddr {
		fmt.Printf("addr= %s\n",addr.String())
	}

}