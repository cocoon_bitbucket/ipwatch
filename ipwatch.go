package ipwatch

import (

	"github.com/garyburd/redigo/redis"
	"time"
	"encoding/json"
	"errors"
	"net"
	"fmt"
	//"sync"
	"strconv"
	//"runtime"
	//"github.com/gammazero/workerpool"
	"strings"
	"context"
	"log"
)

/*

	ipwatcher an agent scanning open tcp ip:ports in a cidr and report to redis

	ipwatch:

	config
	  period: int seconds : period betwwen 2 scans
	  ttl: int second : time to live of the redis key
	  ports[cidr] = []int  ports to watch in a cidr


	ipwatch:setting HASH
	ipwatch:range HASH

	ipwatch:cidr:<cidr>  HASH
	ipwatch:ip:<ip_port> KEY


 */

// cidr eg 192.168.1.1/24
type Ports []int

//
// main configuration  ipwatch:settings ipwatch:range
//
type IpwatchConfig struct {

	Period time.Duration    `toml:"period"`     // period in seconds
	Ttl time.Duration		`toml:"ttl"`        // time to live in seconds
	Timeout int 		    `toml:"timeout"`   // tcp timeout in millisecond
	LogLevel int			`toml:"log_level"`
	IpRanges map[Cidr]Ports	`toml:"ip_ranges"`
}

func ( cnf  * IpwatchConfig) Set(cnx redis.Conn )  (err error){
	// set config to redis db

	// set period and ttl  ipwatch:config
	cnx.Send("MULTI")
	cnx.Send("HSET","ipwatch:setting" , "period" , cnf.Period)
	cnx.Send("HSET","ipwatch:setting" , "ttl" , cnf.Ttl )
	cnx.Send("HSET","ipwatch:setting" , "timeout" , cnf.Timeout )
	cnx.Send("HSET","ipwatch:setting" , "log_level" , cnf.LogLevel)

	// set ranges ipwatch:ranges
	for k,v := range cnf.IpRanges {
		// k is cidr , v is ports
		ports,err := json.Marshal(v)
		if err != nil { return err }
		cnx.Send("HSET","ipwatch:range" , k , ports)
	}
	_, err = cnx.Do("EXEC")
	return err
}

func ( cnf  * IpwatchConfig) Load (cnx redis.Conn )  (err error) {

	dict ,err := redis.StringMap(cnx.Do("HGETALL","ipwatch:setting"))
	if err != nil { return err }

	if len(dict) == 0  {
		err = errors.New("Empty ipwatch:setting")
		return err
	}
	cnf.Period,err = time.ParseDuration(dict["period"])
	if err != nil { return err }
	cnf.Ttl,err = time.ParseDuration(dict["ttl"])
	if err != nil { return err }
	cnf.Timeout,err = strconv.Atoi(dict["timeout"])
	if err != nil { return err }
	cnf.LogLevel,err = strconv.Atoi(dict["log_level"])
	if err != nil { return err }

	// load ipranges from ipwatch:range
	cnf.IpRanges = make(map[Cidr]Ports)

	dict ,err = redis.StringMap(cnx.Do("HGETALL","ipwatch:range"))
	if err != nil { return err }

	if len(dict) == 0  {
		err = errors.New("Empty ipwatch:range")
		return err
	}
	for k,v := range dict{
		ports := []int{}
		err = json.Unmarshal([]byte(v),&ports)
		cnf.IpRanges[Cidr(k)]= ports
		//err = json.Unmarshal([]byte(v),cnf.IpRanges[Cidr(k)])
		if err != nil { return err }
	}
	return err
}

func ( cnf  * IpwatchConfig) AddIpRange (cnx redis.Conn , cidr string , ports []int)  (err error) {

	// *
	key := "ipwatch:range"
	jports,err := json.Marshal(ports)
	if err != nil { return err }
	_,err = cnx.Do("HSET",key , cidr , jports)
	return err
}

func ( cnf  * IpwatchConfig) DelIpRange (cnx redis.Conn , cidr string)  (err error) {
	// *
	key := "ipwatch:range"
	_,err = cnx.Do("HDEL",key , cidr)
	return err
}





//
// ipwatch:cidr:<cidr>    eg ipwatch:cidr:192.168.1.0/24
//
type IpwatchCidrState struct {

	Heartbeat   time.Time // , time.Now())
	Cidr Cidr
    Ports Ports
    Timeout time.Duration
    Eta time.Duration
    Ttl time.Duration
	LogLevel int
}

func NewIpIpwatchCidrState( cidr string,ports []int, timeout int,eta int,ttl int,log_level int) ( s IpwatchCidrState ,err error){

	s = IpwatchCidrState{
		Heartbeat: time.Now(),
		Cidr: Cidr(cidr),
		Ports: Ports(ports),
		Timeout: time.Duration(timeout * 1000000),
		Eta: time.Duration(eta * 1000000000),
		Ttl: time.Duration(ttl * 1000000000),
		LogLevel: log_level,
	}
	return s,err
}

func ( s  * IpwatchCidrState) Set(cnx redis.Conn )  (err error) {
	//
	key := fmt.Sprintf("ipwatch:cidr:%s",s.Cidr)
	r,err := json.Marshal(s)
	_, err = cnx.Do("SET",key,r)

	return err
}

func ( s  * IpwatchCidrState) SetStatus(cnx redis.Conn, cidr Cidr, status string, ttl int )  (err error) {
	//
	key := fmt.Sprintf("ipwatch:cidr:%s:status",s.Cidr)
	_, err = cnx.Do("SETEX",key, ttl , status)
	return err
}

func ( s  * IpwatchCidrState) GetStatus(cnx redis.Conn, cidr Cidr)  (status string, err error) {
	//
	key := fmt.Sprintf("ipwatch:cidr:%s:status",s.Cidr)
	status, err = redis.String(cnx.Do("GET",key))
	return status, err
}

func ( s  * IpwatchCidrState) Load(cnx redis.Conn, cidr string )  (err error) {

	//
	key := fmt.Sprintf("ipwatch:cidr:%s",cidr)
	data,err := redis.Bytes(cnx.Do("GET",key))
	err = json.Unmarshal(data,s)
	return err
}


//
//  ipwatch db
//



type IpwatchDB struct{

	redis.Conn
	Pool redis.Pool
	Config * IpwatchCidrState

	// map of reachable tcp addr
	usedAddr map[string]interface{}

}


//func NewIpWatcher( pool redis.Pool, config * IpwatchConfig) (db IpwatchDB,  err error){
func NewIpWatcher( pool redis.Pool, config * IpwatchCidrState) (db IpwatchDB,  err error){
	//
	//
	//
	cnx := pool.Get()
	addr := make(map[string]interface{})
	db = IpwatchDB{cnx,pool,config,addr}
	_,err = db.Do("PING")
	return db,err
}


func ( db  * IpwatchDB) SetPort( addr * net.TCPAddr, ttl int, response_time time.Duration) (err error) {
	// set port as open for a ttl ttl in seconds
	key := fmt.Sprintf("ipwatch:ip:%s",addr.String())
	_,err = db.Do("SETEX",key,ttl, response_time)
	if err != nil { return err }
	return err
}

func ( db  * IpwatchDB) GetPort( addr * net.TCPAddr ) (err error) {
	// GET a port status , return err if not found
	// if err is nil the port is supposed to be open
	key := fmt.Sprintf("ipwatch:ip:%s",addr.String())
	r,err := redis.Bytes(db.Do("GET",key))
	if err != nil { return err }
	if len(r) == 0 {
		err = errors.New("NotFound")
	}
	return err
}


func (db  * IpwatchDB) SetStatus( status string) (err error) {
	ttl_seconds := int(db.Config.Ttl / 1000000000)
	err = db.Config.SetStatus(db.Conn,db.Config.Cidr,status,ttl_seconds)
	return err
}

func (db  * IpwatchDB) GetStatus() ( status string, err error) {
	status,err = db.Config.GetStatus(db.Conn,db.Config.Cidr)
	if err != nil {
		if strings.Contains(err.Error(),"redigo: nil return") {
			err = nil
			status = "None"
		}
	}
	return status,err
}


func (db  * IpwatchDB) WatchSubnetTask() (err error){

	// take 50s with timeout= 100 millisecond
	// take 4min with timeout = 500 millisecond

	cidr := db.Config.Cidr
	ports := db.Config.Ports
	addrs,err := cidr.RangeNetAddr(ports)
	if err != nil {
		log.Printf("RangeOpenPorts: error: %s",err.Error())
		//time.Sleep(1*time.Second)
		return err
	}

	// save task in db and set status
	err = db.Config.Set(db.Conn)
	time.Sleep(1* time.Millisecond)
	err = db.SetStatus("Running")

	//status,err := db.GetStatus()
	//_=status


	for tcpAddr := range addrs {
		elapsed, err := db.CheckPort(tcpAddr,db.Config.Timeout,db.Config.Ttl,db.Config.LogLevel)
		if err != nil {
			break
		}
		parts := strings.Split(tcpAddr.String(),":")
		if elapsed > 0 {
			// mark address as reachable
			db.usedAddr[tcpAddr.String()]=nil
			// generate positive metric
			TcpConnectionTime.WithLabelValues(parts[0],parts[1]).Set(float64(elapsed))
		} else {
			// timeout
			if _, ok := db.usedAddr[tcpAddr.String()] ; ok {
				// mark address as unused
				delete(db.usedAddr,tcpAddr.String())
				// generetate unreachable metric
				TcpConnectionTime.WithLabelValues(parts[0],parts[1]).Set(0)
			}
		}
		//wp.Submit(checkPortTask(w,tcpAddr,timeout,ttl,log_level))
	}
	//wp.Stop()

	if err == nil {
		// Task OK
		db.SetStatus("Done")
	} else {
		// task KO
		text := err.Error()
		db.SetStatus("FAILED: " + text)
	}
	return err
}

func (db  * IpwatchDB) CheckPort (
	tcpAddr net.TCPAddr, timeout time.Duration, ttl time.Duration, log_level int ) (elapsed time.Duration,err error){
	//
	// try to open port , if OK set port in redis db
	//
	start := time.Now()
	conn, err := net.DialTimeout("tcp", tcpAddr.String(), timeout)
	elapsed = time.Since(start)

	//defer conn.Close()
	if err == nil {

		// produce metrics ok
		//parts := strings.Split(tcpAddr.String(),":")
		//TcpConnectionTime.WithLabelValues(parts[0],parts[1]).Set(float64(elapsed))

		if log_level >= 10 {
			fmt.Printf("-- checkaddr: CONNECTION OK  to %s\n", tcpAddr.String())
		}
		// port is open => register in db
		conn.Close()
		ttl_seconds := int(ttl / 1000000000)
		err = db.SetPort( &tcpAddr,ttl_seconds,elapsed)
		if err != nil {
			return elapsed, err
		}
	} else {
		// error while dialing has occurred (timeout ..)
		err = nil
		elapsed = 0

		// produce metrics KO
		//parts := strings.Split(tcpAddr.String(),":")
		//TcpConnectionTime.WithLabelValues(parts[0],parts[1]).Set(0)

		if log_level >= 90 {
			fmt.Printf("-- fail to connect to %s\n", tcpAddr.String())
		}
	}
	// dont return the tcp status
	return elapsed,nil
}



func checkPort( w  * IpwatchDB, tcpAddr net.TCPAddr, timeout time.Duration, ttl int, log_level int ) (err error){
    //
	// try to open tcp port , if OK set port in redis db
	//
	start := time.Now()
	conn, err := net.DialTimeout("tcp", tcpAddr.String(), timeout)
	elapsed := time.Since(start)

	//defer conn.Close()
	if err == nil {
		if log_level >= 10 {
			fmt.Printf("-- checkaddr: CONNECTION OK  to %s\n", tcpAddr.String())
		}
		// port is open => register in db
		conn.Close()
		err = w.SetPort( &tcpAddr,ttl,elapsed)
		if err != nil {
			return err
		}
	} else {
		if log_level >= 90 {
			fmt.Printf("-- fail to connect to %s\n", tcpAddr.String())
		}
	}
	// dont return the tcp status
	return nil
}


func checkPortTask(  w  * IpwatchDB, tcpAddr net.TCPAddr, timeout time.Duration, ttl int, log_level int) func() {
	// a curry for checkAddr
	wrapper := func() {
		checkPort( w,tcpAddr,timeout,ttl,log_level)
	}
	return wrapper
}



func GetDbIpwatchIp(cnx redis.Conn, cidr Cidr) ( ips []string, err error ) {

	// scan db for keys like  ipwatch:ip:192.168.1.*  ( open ports )
	//
	// return list of ip like  192.168.1.1:23, ...

	ipwatch_key := "ipwatch:ip:"

	ip, ipnet, err := net.ParseCIDR(string(cidr))
	if err != nil { return nil, err }

	// get first ip of subnet
	first := ip.Mask(ipnet.Mask)
	pattern := ""
	for i:=0 ; i<3 ;i++{
		e:= first[i]
		v := strconv.Itoa(int(e))
		pattern = pattern + v + "."
	}
	key_pattern := ipwatch_key + pattern + "*"

	keys,err := redis.Values(cnx.Do("KEYS",key_pattern))
	if err != nil { return ips,err}

	for _,key := range keys {
		k :=  string(key.([]uint8))
		addr := k[ len(ipwatch_key):]
		ips = append(ips,addr)
	}
	return ips,err
}


func RangeOpenPorts( cnx redis.Conn,  cidr Cidr, ports Ports) ( netAddr chan net.TCPAddr, err error) {
	//
	// get a channel of open ports
	//
	c := make(chan net.TCPAddr)

	// get list of open ports from db
	db_open_ports,err := GetDbIpwatchIp(cnx,cidr)
	if err != nil { return netAddr,err}

	addr_list,err := cidr.RangeNetAddr(ports)
	if err != nil {
		log.Printf("RangeOpenPorts: error: %s",err.Error())
		return netAddr,err
	}

	go func() {
		defer close(c)

		for addr := range addr_list {

			// if addr in db_open_ports
			found := false
			saddr := addr.String()
			for _,ref := range db_open_ports {
				if saddr == ref {
					found = true
					break
				}
			}
			if found {
				c <-  addr
			}
		}
	}()
	return c,err
}




//
//    IpwatchManager
//

/*
	manage Ipwatchers

	periodicaly scan configuration ipwatch:range to launch a Ipwatcher for each subnet declared


 */


type IpwatchManager struct {

	Pool redis.Pool
	Period time.Duration
	LogLevel int

}


func NewIpwatchManager(pool redis.Pool, period time.Duration, logLevel int) ( m IpwatchManager ,err error ){

	/*

		ipwatch:manager HASH  { status: started }

	 */

	if period == 0 {
		period = 120 * time.Second
	}
	m.Period = period
	cnx := pool.Get()
	defer cnx.Close()

	// check connection
	_,err= cnx.Do("PING")
	if err != nil { return m,err }
	cnx.Close()

	m = IpwatchManager{pool,period,logLevel}

	// launch the manager service
	//go func() {
	//	m.Run()
	//}()

	return m,err
}


func (m * IpwatchManager) heartbeat() {
	// write a timestamp to ipwatch:heartbeat

	cnx := m.Pool.Get()
	defer cnx.Close()

	h := IpwatchHelper{cnx}
	h.SetHeartbeat()

	//t := time.Now()
	//cnx.Do("SET","ipwatch:heartbeat",t)

}


func (m * IpwatchManager) Run () {

	//// get a connection from pool for this cycle
	//cnx := m.Pool.Get()
	//// test it
	//_,err := cnx.Do("PING")
	//
	//if err != nil {
	//	// no cnx : will wait and retry
	//	fmt.Printf("Cannot get a connection from pool: %s\n",err)
	//	return
	//}

	// setup a context with a event channel
	Events := make(chan error)
	ctx := context.Background()
	ctx = context.WithValue(ctx,"Events",Events)

	// forever loop
	for {

		// cycle loop
		for {
			// try to launch a task for each subnet

			m.heartbeat()
			fmt.Printf("IpwatchManager: Start a new cycle\n")

			cnf := IpwatchConfig{}
			cnx := m.Pool.Get()
			err := cnf.Load(cnx)
			if err != nil {
				fmt.Printf("IpwatchManager: cannot load ipwatch configuration :%s\n", err.Error())
				break
			}
			cnx.Close()

			subnets := cnf.IpRanges

			if len(subnets) == 0 {
				fmt.Printf("IpwatchManager: No subnet to scan\n")
				break
			}
			// for each subnet
			for subnet, ports := range subnets {
				_=ports

				m.Spawn(ctx,subnet,cnf)

				// need cooldown tempo
				time.Sleep(1*time.Second)
			}
			break
		}

		// a loop
		fmt.Printf("IpwatchManager: End of cycle, wait for %s\n",m.Period )
		for end := time.Now().Add(m.Period); end.Sub(time.Now()) > 0 ;{

			select {
			//case res := <-c1:
			//	fmt.Println(res)
			case <-time.After(1 * time.Second):
				//fmt.Printf("1 seconds\n")
				m.heartbeat()
				continue

			case <- Events:
				// we receive an event from a child task
				fmt.Printf("IpwatchManager receive an Events message from task\n")
				break
			}
			break

		}
		//cnx.Close()
		//fmt.Printf("IpwatchManager: End of cycle, wait for %s\n",m.Period )
		//time.Sleep(m.Period)
	}
}


func (m * IpwatchManager) Spawn( ctx context.Context, cidr Cidr , cnf IpwatchConfig ) {
	// spawn an ip watcher for the given cidr
	go func () {

		// set task configuration for ipwatcher
		ports:= cnf.IpRanges[cidr]
		log_level := cnf.LogLevel
		task,err := NewIpIpwatchCidrState(string(cidr),ports,cnf.Timeout,0,0,log_level)
		task.Eta = task.Timeout * time.Duration(len(ports) * 256)
		task.Ttl = task.Eta * 2

		//fmt.Printf("timeout: %s\n",task.Timeout)
		//fmt.Printf("eta: %s\n", task.Eta)
		//fmt.Printf("ttl: %s\n",task.Ttl)

		fmt.Printf("IpwatchManager: launch watchSubnetTask for %s ,timeout=%s,eta=%s,ttl=%s\n",
			cidr,task.Timeout,task.Eta,task.Ttl)

		watcher,err := NewIpWatcher(m.Pool, &task)
		defer watcher.Close()
		if err != nil {
			fmt.Printf("WatchSubnetTask cannot create watcher: %s\n",err)
			return
		}

		// check there is no running task on this subnet
		status,err := watcher.GetStatus()
		if err == nil {
			switch status {
			case "Running":
				fmt.Printf("WatchSubnetTask busy subnet: %s , will try again later...\n",cidr)
				return
			}
		} else {
			// error
			fmt.Printf("WatchSubnetTask cannot read status for this subnet: %s,err:%s\n",cidr,err)
			return
		}
		//err = WatchSubnetTask( &watcher, cnf, cidr)
		err = watcher.WatchSubnetTask()
		if err != nil {
			fmt.Printf("WatchSubnetTask for subnet %s has return error: %s\n",cidr,err)
			time.Sleep( 60 * time.Second)
		}
		fmt.Printf("watchSubnetTask for %s has returned\n",cidr)
		// send a message to the Events channel to tell Im finished
		ch,ok := ctx.Value("Events").(chan error)
		if ok {
			ch <- nil
		}
		return
	}()

}


//
//    ipwatchClient
//

type IpwatchClient struct {

	Pool redis.Pool
}

type IpwatchReader interface {

	IsAlive() (bool,error)
	//IsSubnet(cidr string) error
	AddIpRange(cidr string, ports []int) error
	DelIpRAnge(cidr string)
	// return
	RangeOpenPorts(cidr string,ports[]int) ( chan net.TCPAddr, error )

}

//func ( c * IpwatchClient) is_subnet( cnx redis.Conn,cidr string) ( err error) {
//	// check if subnet is supervised  ipwatch:cidr:<cidr>
//	key := "ipwatch:cidr:" + cidr
//	_
//	return err
//}


func ( c * IpwatchClient ) is_manager_alive(cnx redis.Conn) (bool, error){

	h := IpwatchHelper{cnx}

	elapsed := 0 * time.Second
	t0,err := h.GetHeartbeat()
	if err == nil {
		time.Sleep( 2100 * time.Millisecond)
		t1, err := h.GetHeartbeat()
		if err == nil {
			elapsed = t1.Sub(t0)
			fmt.Printf("elapsed:%s\n",elapsed)
			if elapsed > 0 {
				return true,nil
			}
		}
	}
	return false,err
}


func ( c * IpwatchClient) IsAlive() ( alive bool, err error ) {
	cnx := c.Pool.Get()
	defer cnx.Close()
	return c.is_manager_alive(cnx)
}


func ( c * IpwatchClient) RangeOpenPorts( cidr string, ports []int) ( addrChan chan net.TCPAddr, err error ) {
	//
	// return a channel of net.TCPAddr of open ports
	//

	cnx := c.Pool.Get()
	defer cnx.Close()

	//h := IpwatchHelper{cnx}

	// check subnet is supervised ipwatch:cidr:<cidr>
	key := "ipwatch:cidr:" + cidr
	r,err := cnx.Do("GET",key)
	if err != nil { return addrChan,err }
	_=r

	// check a ipwatch_manager is alive
	//alive,err := c.is_manager_alive(cnx)
	//if ! alive {
	//	if err != nil {
	//		err = errors.New("cannot check if ipwatch server is running (%s)" + err.Error())
	//	} else {
	//		err = errors.New("ipwatch server not running or freezed")
	//	}
	//	return addrChan,err
	//}
	return RangeOpenPorts(cnx,Cidr(cidr),ports)
}

func ( c * IpwatchClient) AddIpRange (cidr string, ports []int  ) ( err error ) {
	// add an ip range for the server
	cnx := c.Pool.Get()
	defer cnx.Close()
	h := IpwatchHelper{cnx}
	return h.AddIpRange(cidr,ports)
}

func ( c * IpwatchClient) DelIpRange (cidr string) ( err error ) {
	// add an ip range for the server
	cnx := c.Pool.Get()
	defer cnx.Close()
	h := IpwatchHelper{cnx}
	return h.DelIpRange(cidr)
}


//
// utils
//

type IpwatchHelper struct {
	cnx redis.Conn
}

func (h *IpwatchHelper) SetHeartbeat() error {
	t := time.Now().Format(time.RFC3339Nano)
	_,err := h.cnx.Do("SET","ipwatch:heartbeat",t)
	return err
}

func (h *IpwatchHelper) GetHeartbeat() ( timestamp time.Time,err  error) {
	r,err := redis.String(h.cnx.Do("GET","ipwatch:heartbeat"))
	if err != nil { return timestamp,err}
	timestamp,err = time.Parse( time.RFC3339Nano,r)
	return timestamp,err
}


func (h *IpwatchHelper) AddIpRange ( cidr string , ports []int)  (err error) {
	// *

	// check cidr
	err = CheckCidr(cidr)
	if err != nil {return err}

	key := "ipwatch:range"
	jports,err := json.Marshal(ports)
	if err != nil { return err }
	_,err = h.cnx.Do("HSET",key , cidr , jports)
	return err
}

func (h *IpwatchHelper) DelIpRange ( cidr string)  (err error) {
	// *
	key := "ipwatch:range"
	_,err = h.cnx.Do("HDEL",key , cidr)
	return err
}



func GetKeys( pool redis.Pool ,pattern string) ([]string, error) {

	conn := pool.Get()
	defer conn.Close()

	iter := 0
	keys := []string{}
	for {
		arr, err := redis.Values(conn.Do("SCAN", iter, "MATCH", pattern))
		if err != nil {
			return keys, fmt.Errorf("error retrieving '%s' keys", pattern)
		}
		iter, _ = redis.Int(arr[0], nil)
		k, _ := redis.Strings(arr[1], nil)
		keys = append(keys, k...)
		if iter == 0 {
			break
		}
	}
	return keys, nil
}
