package ipwatch_test



import(

	"testing"
	"github.com/alicebob/miniredis"
	"github.com/garyburd/redigo/redis"
	"github.com/stretchr/testify/assert"

	"time"
)



func TestMiniredis(t *testing.T) {


	s, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	defer s.Close()

	// Optionally set some keys your code expects:
	s.Set("foo", "bar")
	s.HSet("some", "other", "key")


	addr:= s.Addr()
	_=addr

	// Run your code and see if it behaves.
	// An example using the redigo library from "github.com/garyburd/redigo/redis":
	c, err := redis.Dial("tcp", s.Addr())
	_, err = c.Do("SET", "foo", "bar")

	// Optionally check values in redis...
	if got, err := s.Get("foo"); err != nil || got != "bar" {
		t.Error("'foo' has the wrong value")
	}
	// ... or use a helper for that:
	s.CheckGet(t, "foo", "bar")

	// TTL and expiration:
	s.Set("foo", "bar")
	s.SetTTL("foo", 10*time.Second)
	s.FastForward(11 * time.Second)
	if s.Exists("foo") {
		t.Fatal("'foo' should not have existed anymore")
	}


}


func TestMockRedis(t *testing.T) {


	mock := NewMockRedis()
	defer mock.Close()


	c, err := redis.Dial("tcp", mock.Addr())
	assert.Equal(t,nil,err)


	_, err = c.Do("SET", "foo", "bar")
	// Optionally check values in redis...
	if got, err := mock.Get("foo"); err != nil || got != "bar" {
		t.Error("'foo' has the wrong value")
	}
	// ... or use a helper for that:
	mock.CheckGet(t, "foo", "bar")

	// TTL and expiration:
	mock.Set("foo", "bar")
	mock.SetTTL("foo", 10*time.Second)
	mock.FastForward(11 * time.Second)
	if mock.Exists("foo") {
		t.Fatal("'foo' should not have existed anymore")
	}

	pool := mock.Pool()
	cnx := pool.Get()
	defer cnx.Close()


}